//
// Created by viktorm
//

#ifndef SF2568_IOBUS_H
#define SF2568_IOBUS_H

#include <vector>
#include <cstdint>
#include <mpi.h>
#include <memory>
#include "Config.h"
#include <algorithm>

// Typedefs
typedef std::vector<uint8_t> ARCHIVE;
typedef std::pair<MPI_Request, std::vector<uint8_t>> TSEND;
typedef std::pair<MPI_Status, std::vector<uint8_t>> TRECV;

/**
 * IOBus for simplified communcation
 */
struct IOBus {

    // Outgoing buffers
    std::vector<std::shared_ptr<TSEND>> outgoing;

    /**
     * Send data to all processors
     * @param data to send
     * @param root processor
     * @param tag or channel to use
     */
    void SendAll(std::vector<uint8_t> &&data, int root, int tag = MPI_ANY_TAG) {
        // Iterate over all processors
        for (int i = 0; i < Config::NPROC; i++) {
            // Skip the root processor
            if (i == root) continue;
            // Emplace into outgoing buffer
            auto &ref = outgoing.emplace_back(std::make_shared<TSEND>(std::make_pair(MPI_Request(), data)));
            // Initiate asynchronous send
            MPI_Isend(ref->second.data(), ref->second.size(), MPI_BYTE, i, tag, MPI_COMM_WORLD, &(ref->first));
        }
    }

    /**
     * Send data to all processors
     * @param data to send
     * @param destination processor
     * @param tag or channel to use
     */
    void Send(std::vector<uint8_t> &&data, int destination, int tag = MPI_ANY_TAG) {
        // Emplace into outgoing buffer
        auto &ref = outgoing.emplace_back(std::make_shared<TSEND>(std::make_pair(MPI_Request(), std::move(data))));
        // Initiate asynchronous send
        MPI_Isend(ref->second.data(), ref->second.size(), MPI_BYTE, destination, tag, MPI_COMM_WORLD, &(ref->first));
    }

    /**
     * Update the IOBus, checking receive and transfer statuses
     * @param source processor, default MPI_ANY_SOURCE
     * @param tag or channel, default MPI_ANY_TAG
     * @return incoming data archive(s)
     */
    std::vector<std::shared_ptr<TRECV>> Update(int source = MPI_ANY_SOURCE, int tag = MPI_ANY_TAG) {

        {
            // Iterate over outgoing buffer, check if outgoing buffer transfer has completed, if so remove it
            auto end = std::remove_if(outgoing.begin(),
                                      outgoing.end(),
                                      [](const std::shared_ptr<TSEND> &element) {
                                          int flag;
                                          MPI_Status status;
                                          // Test if outgoing buffer is completed
                                          MPI_Test(&element->first, &flag, &status);
                                          return flag != 0;
                                      });
            // Erase completed buffers
            outgoing.erase(end, outgoing.end());
        }

        // Create incoming data buffers
        std::vector<std::shared_ptr<TRECV>> incomingData;

        {
            int flag;
            do {
                MPI_Status status;
                // Probe to see if there is available incoming data
                MPI_Iprobe(source, tag, MPI_COMM_WORLD, &flag, &status);
                if (flag != 0) {
                    int count;
                    // Check how many incoming bytes
                    MPI_Get_count(&status, MPI_BYTE, &count);
                    // Allocate incoming data buffer
                    auto &ref = incomingData.emplace_back(
                            std::make_shared<TRECV>(std::make_pair(MPI_Status(), std::vector<uint8_t>(count))));
                    // Recieve into data buffer
                    MPI_Recv(ref->second.data(), count, MPI_BYTE, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD,
                             &ref->first);
                }
            } while (flag != 0); // Continue until there is no more data available
        }

        // Return incoming data
        return incomingData;
    }
};

/**
 * IOBatch to coalesce archives
 */
struct IOBatch {

    const int tag = -1;
    std::vector<std::pair<int, std::shared_ptr<TSEND>>> outgoing;

    /**
     * Construct an IOBatch
     * @param tag or channel for batch
     */
    explicit IOBatch(int tag) : tag(tag) {};

    /**
     * Batch data to all processors
     * @param data to send
     * @param root processor
     */
    void SendAll(std::vector<uint8_t> &&data, int root) {
        for (int i = 0; i < Config::NPROC; i++) {
            if (i == root) continue;

            // Check if there is already a buffer in use
            auto it = std::find_if(outgoing.begin(), outgoing.end(),
                                   [&](const std::pair<int, std::shared_ptr<TSEND>> &element) {
                                       return element.first == i;
                                   });

            if (it != outgoing.end()) {
                // If there is a buffer in use, re-use it by appending
                it->second->second.insert(std::end(it->second->second), std::begin(data), std::end(data));
            } else {
                // If there is no buffer in use, create a new one
                auto &ref = outgoing.emplace_back(i,
                                                  std::make_shared<TSEND>(
                                                          std::make_pair(MPI_Request(), data)));
            }
        }
    }

    /**
     * Send data to specific processor
     * @param destination processor
     * @return archive for transmission
     */
    std::vector<uint8_t> &Send(int destination) {
        // Check if there is already a buffer in use
        auto it = std::find_if(outgoing.begin(), outgoing.end(),
                               [&](const std::pair<int, std::shared_ptr<TSEND>> &element) {
                                   return element.first == destination;
                               });

        if (it != outgoing.end()) {
            // If there is a buffer in use, re-use it
            return it->second->second;
        } else {
            // If there is no buffer in use, create a new one
            auto &ref = outgoing.emplace_back(destination,
                                              std::make_shared<TSEND>(
                                                      std::make_pair(MPI_Request(), std::vector<uint8_t>())));
            return ref.second->second;
        }
    }

    /**
     * Commit the batch and send it using specific IOBus object
     * @param bus to use for transmission
     */
    void Commit(IOBus &bus) {
        // Iterate over all batches
        for (auto &batch : outgoing) {
            // Create an outgoing buffer in bus object
            auto &ref = bus.outgoing.emplace_back(std::move(batch.second));
            // Initiate transmission on outgoing buffer
            MPI_Isend(ref->second.data(), ref->second.size(), MPI_BYTE, batch.first, tag, MPI_COMM_WORLD,
                      &(ref->first));
        }
    }

};

#endif //SF2568_IOBUS_H
