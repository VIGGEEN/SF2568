//
// Created by Viktor Meyer
//

#ifndef SF2568_ENTITY_H
#define SF2568_ENTITY_H

#include <vector>
#include <algorithm>
#include <cstdint>
#include <sstream>
#include <chrono>
#include <thread>
#include <cassert>
#include "Config.h"
#include "Utility.h"

/**
 * Command struct containing client input
 */
struct __attribute__((packed)) Command {
    uint64_t identifier;
    bool forward;
    bool backward;
    bool right;
    bool left;
};

/**
 * State struct containing entity state per tick
 */
struct __attribute__((packed)) State {

    // Entity identifier associated with state
    uint64_t identifier;

    // State coordinate-x
    double x;

    // State coordinate-y
    double y;

    /**
     * Simulate a new state by applying input to current state
     * @param state current
     * @param cmd input
     * @return new state based on input and current state
     */
    static State Simulate(State state, const Command &cmd) {

        // Check if FORWARD key is down
        if (cmd.forward) {
            // Change coordinate-y
            state.y -= 1.f;
        }

        // Check if BACKWARD key is down
        if (cmd.backward) {
            // Change coordinate-y
            state.y += 1.f;
        }

        // Check if RIGHT key is down
        if (cmd.right) {
            // Change coordinate-x
            state.x += 1.f;
        }

        // Check if LEFT key is down
        if (cmd.left) {
            // Change coordinate-x
            state.x -= 1.f;
        }

        // Clamp position to make sure we're within world bounds
        Clamp(state);
        assert(state.x >= 0 && state.x < Config::DIM);
        assert(state.y >= 0 && state.y < Config::DIM);

        // Return the new state
        return state;
    }

    /**
     * Clamp state within world bounds
     * @param state clamped to world bounds
     */
    static void Clamp(State &state) {
        state.x = std::clamp(state.x, 0.0, static_cast<double>(Config::DIM - 1));
        state.y = std::clamp(state.y, 0.0, static_cast<double>(Config::DIM - 1));
    }
};

/**
 * Base entity simulation struct
 */
struct Entity {

    bool primary = true;
    std::vector<int> secondaries;

    // Unique entity identifier
    uint64_t identifier;

    // Command buffer
    std::vector<Command> commandBuffer;

    // Currently executed command
    Command currentCommand;

    // Current state
    State currentState;

    // Next predicted state
    State predictedState;

    /**
     * Constructs an entity
     * @param identifier of the entity
     */
    explicit Entity(uint64_t identifier) {
        this->identifier = identifier;
        currentState.identifier = this->identifier;
    }

    /**
     * Calculates the squared distance to another entity
     * @param other entity
     * @return squared distance to another entity (d*d)
     */
    inline double distance(Entity &other) {
        const auto cx = (currentState.x - other.currentState.x);
        const auto cy = (currentState.y - other.currentState.y);
        return cx * cx + cy * cy;
    }

    /**
     * Evaluates whether another entity is within area-of-interest
     * @param other entity
     * @return whether other entity is within area-of-interest
     */
    inline bool isAOI(Entity &other) {
        return distance(other) <= Config::ENTITYSIZE * Config::ENTITYSIZE;
    }

    /**
     * Simulates the entity by one tick
     */
    void Tick() {
        // Sleep for a number of microseconds, to emulate a real world scenario
        std::this_thread::sleep_for(std::chrono::microseconds(Config::EXTRAWORK));

        // Check if we have a new command available
        if (!commandBuffer.empty()) {
            // Update current command
            currentCommand = commandBuffer.front();
            // Shift the command buffer
            pop_front(commandBuffer, 1);
        }

        // Simulate the entity
        currentState = State::Simulate(currentState, currentCommand);
    }

    // Predict the state of this entity for the next tick
    State Predict() {
        predictedState = State::Simulate(currentState, currentCommand);
        return predictedState;
    }

};

#endif //SF2568_ENTITY_H