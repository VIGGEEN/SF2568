//
// Created by viktorm
//

#ifndef SF2568_CONNECTEDCOMPONENTS_H
#define SF2568_CONNECTEDCOMPONENTS_H

#include <iostream>
#include <list>
#include <vector>
#include <dynamic_bitset.hpp>

/**
 * Basic graph structure used for finding Weakly Connected Components (WCC)
 * Inspired by https://www.geeksforgeeks.org/connected-components-in-an-undirected-graph/
 */
namespace WCC {

    // Index data type
    typedef uint64_t INDEX;

    // Basic graph structure
    class Graph {
        const INDEX V;
        dynamic_bitset<> adjacencyMatrix;

        // Depth first search visitor
        void DFS(INDEX v, dynamic_bitset<>& visited, dynamic_bitset<> &result) {
            // Add the current vertex to result set
            result.set(v,true);
            // Find first adjacent vertex
            auto ipos = adjacencyMatrix.find_next(v * V);
            while (ipos < v*V+V && ipos != dynamic_bitset<>::npos){
                // Compute index based on offset
                const auto index = ipos - (v * V);
                // If not visited, recursively visit and set visited
                if (!visited.test_set(index, true)) DFS(index, visited, result);
                // Find the next adjacent vertex
                ipos = adjacencyMatrix.find_next(ipos);
            }
        }

    public:

        // Create a new graph, max size V
        explicit Graph(INDEX V) : V(V), adjacencyMatrix(V * V) {}

        // Insert an edge into the graph
        inline void insertEdge(INDEX v, INDEX w) {
            // To simulate undirected edge, add edge in both directions
            adjacencyMatrix.set(v * V + w, true);
            adjacencyMatrix.set(w * V + v, true);
        }

        // Compute weakly connected components
        std::vector<dynamic_bitset<>> ComputeComponents() {
            // Construct visited set
            dynamic_bitset<> visited = dynamic_bitset<>(V);
            // Construct result components
            std::vector<dynamic_bitset<>> result;
            // Iterate over all vertices
            for (int v = 0; v < V; v++) {
                // For each unvisted vertex, visit recursively
                if (!visited.test_set(v,true)) DFS(v, visited, result.emplace_back(V));
            }
            // Return weakly connected components
            return result;
        }
    };
}

#endif //SF2568_CONNECTEDCOMPONENTS_H
