//
// Created by viktorm
//

#include <cmath>
#include <cassert>
#include "Config.h"

size_t Config::NPROC = 0;
size_t Config::NAXIS = 0;
size_t Config::CELLSIZE = 0;

void Config::Init(size_t nrpoc) {
    // Make sure that the program was initialized with correct mpirun number of processes
    assert(Config::NPROC % 4 == 0 || Config::NPROC == 2);
    // Initialize global variables
    Config::NPROC = nrpoc;
    Config::NAXIS = std::sqrt(nrpoc);
    Config::CELLSIZE = (DIM / (NAXIS));
}
