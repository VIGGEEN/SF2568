//
// Created by viktorm
//

#include "ClientEmulator.h"

/**
 * Constructs a client emulator
 */
ClientEmulator::ClientEmulator() = default;

/**
 * Create an entity
 */
void ClientEmulator::CreateEntity() {

    // Create an IOBatch for transmission
    IOBatch batch(Config::CH_PRIMARYENTITY);

    // Create an entity and serialize into batch
    CreateEntity(batch);

    // Commit the batch and transmit it to server
    batch.Commit(io);

}

/**
 * Create an entity
 * @param batch to serialize into
 */
void ClientEmulator::CreateEntity(IOBatch &batch) {
    // Monotonically increasing identifier to avoid collision
    static uint64_t id = 0;
    // Create a new entity
    Entity ent(id++);
    // Initialize the current state
    ent.currentState.identifier = ent.identifier;
    ent.currentState.x = randomcoord();
    ent.currentState.y = randomcoord();
    // Clamp the current state coordinates
    State::Clamp(ent.currentState);
    // Allocate an archive to serialize into
    auto &archive = batch.Send(DynamicPartition::Master());
    // Serialize entity data into archive
    SerializeEntity(&ent, archive, archive.size());
    // Insert the entity state into local buffer
    entities.emplace_back(ent.currentState);
    // Generate a random target for the entity
    entityTargets.emplace_back(randomcoord(), randomcoord());
}

/**
 * Processes IO by checking IOBus for new data and sending buffered data
 */
void ClientEmulator::ProcessIO() {
    // Check IOBus for new data and sending buffered data
    auto incomingData = io.Update();
    // Process all incoming data
    for (auto &archive : incomingData) {
        //Check if tag is CH_STATE (state update)
        if (archive->first.MPI_TAG == Config::CH_STATE) {
            int offset = 0;
            while (offset < archive->second.size()) {
                State state;
                // Deserialize state update
                offset += Deserialize<State>(&state, archive->second, offset);
                // Find entity with binary search in local buffer
                auto it = std::lower_bound(entities.begin(), entities.end(), state,
                                           [](const State &lhs, const State &rhs) {
                                               return lhs.identifier < rhs.identifier;
                                           });
                // Update the local entity with the state update
                *it = state;
            }
        }
    }
}

/**
 * Generates and sends commands for each entity that moves the entity towards its target position
 */
void ClientEmulator::GenerateCommands() {

    // Create an IOBatch to serialize commands into
    IOBatch batch(Config::CH_COMMAND);

    // Iterate over all local entities
    for (size_t i = 0; i < entities.size(); i++) {
        // Get the current data
        auto &currentState = entities[i];
        auto &currentTarget = entityTargets[i];
        // Generate a command with input that moves the entity towards its target
        Command cmd = {
                currentState.identifier,
                ((currentTarget.second - currentState.y) < -5) /* Check delta to see if input FORWARD */,
                ((currentTarget.second - currentState.y) > 5) /* Check delta to see if input BACKWARD */,
                ((currentTarget.first - currentState.x) > 5) /* Check delta to see if input RIGHT */,
                ((currentTarget.first - currentState.x) < -5) /* Check delta to see if input LEFT */
        };

#ifdef DYNAMIC
        // Allocate an archive to serialize into, for DYNAMIC mode, target is master for LPT scheduling to occur
        auto &ar = batch.Send(DynamicPartition::Master());

        //Serialize the command into the archive
        Serialize(&cmd, ar);
#else
        // Allocate an archive to serialize into
        ARCHIVE ar;

        //Serialize the command into the archive
        Serialize(&cmd, ar);

        // Set the archive to be sent to all, the client can not know on which process the entity actually resides
        batch.SendAll(std::move(ar), Config::NPROC);
#endif
    }

    // Commit the batch and transmit it to server
    batch.Commit(io);
}

/**
 * Generates target positions for each entity
 */
void ClientEmulator::GenerateTargets() {
    static double targetExpiry = 0;
    // Check if the current targets have expired
    if (MPI_Wtime() > targetExpiry) {
        // Iterate over all targets
        for (size_t i = 0; i < entityTargets.size(); i++) {
            // Generate a new target and update the current entities target
            entityTargets[i] = std::make_pair(randomcoord(), randomcoord());
        }
        // Set the newly updated targets expiry
        targetExpiry = MPI_Wtime() + Config::TARGETTIMEOUT;
    }
}

/**
 * Client main-loop
 */
void ClientEmulator::ClientLoop() {
    static auto initTime = MPI_Wtime();
    static auto lastClientLoopTime = MPI_Wtime();
    static auto lastCreatedEntityTime = lastClientLoopTime;
    static int numCreatedEntitites = 0;
    while (true) {
#ifndef SERIAL
        // Synchronize all processors
        MPI_Barrier(MPI_COMM_WORLD);
#endif
        // Check IOBus for new data and sending buffered data
        ProcessIO();

        // Make sure to create an appropriate number of entities
        while (numCreatedEntitites < (MPI_Wtime() - lastCreatedEntityTime) * Config::TICKRATE &&
               numCreatedEntitites < Config::ENTITIES) {
            CreateEntity();
            numCreatedEntitites++;
        }

        // Check if enough time has passed to update
        if (MPI_Wtime() > lastClientLoopTime + (1.0 / Config::TICKRATE)) {
            // If we have created all entities needed for emulation, exit with non-zero to cause all processes exit
            if (numCreatedEntitites >= Config::ENTITIES) {
                std::cout << "SUCCESSFUL SIMULATION, USING EXIT(1) TO FORCE QUIT ALL PROCESSES" << std::endl;
                exit(1);
            }
            // Generate and send commands for each entity that moves the entity towards its target position
            GenerateCommands();
            // Generate target positions for each entity
            GenerateTargets();
            // Update the lastClientLoopTime to current
            lastClientLoopTime = MPI_Wtime();
        }
        #ifndef SERIAL
        #ifdef DYNAMIC
        // Synchronize all processors
        MPI_Barrier(MPI_COMM_WORLD);
        #endif
        #endif
    }
}

/**
 * Generates a random 1D-coordinate within the world bounds
 * @return random 1D-coordinate
 */
double ClientEmulator::randomcoord() {
    // Create a generator engine
    static std::default_random_engine generator(2568);
    #ifdef SKEWED
    // Use gamma distribution to generate skewed data
    static std::gamma_distribution<double> distribution(1.5, 0.15);
    #elif HOTSPOT
    // Use normal distribution to generate centered hotspot data
    static std::normal_distribution<double> distribution(0.5, 0.05);
    #else
    // Use uniform distribution to generate uniform data
    static std::uniform_real_distribution<double> distribution(0, 1);
    #endif
    // Scale the generated value by world bounds
    return distribution(generator) * Config::DIM;
}
