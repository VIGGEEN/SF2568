//
// Created by viktorm
//

#ifndef SF2568_DYNAMICPARTITION_H
#define SF2568_DYNAMICPARTITION_H

#include "IOBus.h"
#include "Entity.h"
#include "Serialization.h"
#include "ConnectedComponents.h"
#include <fstream>
#include <cmath>

class DynamicPartition {
public:

    // Partition rank
    int rank;

    // Output stream for logging
    std::ofstream log;

    // IOBus for inter-processor communication
    IOBus io;

    // Local buffer for entities
    std::vector<Entity> _entities;

    // Current partition tick
    uint32_t worldTick = 0;

    // Partition initialization time
    double start_time;

    /**
     * Constructs a partition
     * @param rank of partition
     */
    explicit DynamicPartition(int rank);

    /**
     * Destructor
     */
    ~DynamicPartition();

    /**
     * Processes IO by checking IOBus for new data and sending buffered data
     */
    void ProcessIO();

    /**
     * Server main-loop
     */
    void ServerLoop();

    /**
     * Simulates the partition one tick
     */
    void Tick();

    /**
     * Gets the master process rank
     * @return master process rank
     */
    static int Master();

    /**
     * Whether the current partition is running on master process
     * @return true if current partition is running master process
     */
    bool IsMaster() const;

    /**
     * Time elapsed since partition initialization
     * @return time elapsed since partition initialization
     */
    double ElapsedTime() const;

    /**
     * The target tick based on time elapsed, where the partition worldTick **should** be at
     * @return where the partition worldTick **should** be at
     */
    uint32_t TargetTick() const;
};


#endif //SF2568_DYNAMICPARTITION_H
