//
// Created by viktorm
//

#ifndef SF2568_SERIALIZATION_H
#define SF2568_SERIALIZATION_H

#include <cstring>
#include "Entity.h"
#include "Utility.h"

/**
 * Serialize trival types into bytes
 * @tparam T type to serialize
 * @param src to serialize
 * @param archive to serialize into
 * @param offset in archive to serialize into
 * @return size of the object that was serialized in bytes
 */
template<typename T>
static size_t Serialize(const T *src, std::vector<uint8_t> &archive, int offset = -1) {
    static_assert(std::is_trivial<T>::value); // Ensure type is trivial
    // Check if offset has been specified or not
    if (offset == -1) {
        offset = archive.size();
        // Resize the archive to make size for serialization
        archive.resize(archive.size() + sizeof(T));
    }
    // Copy the type into the archive
    memcpy(archive.data() + offset, src, sizeof(T));
    // Return the size of the type
    return sizeof(T);
}

/**
 * Deserialize trivial types from bytes
 * @tparam T type to serialize
 * @param result to serialize into
 * @param archive to deserialize from
 * @param offset in archive to deserialize from
 * @return size of the object that was deserialized in bytes
 */
template<typename T>
static size_t Deserialize(T *result, std::vector<uint8_t> &archive, int offset) {
    static_assert(std::is_trivial<T>::value); // Ensure type is trivial
    // Copy the bytes from archive into the result
    memcpy(result, archive.data() + (offset == -1 ? 0 : offset), sizeof(T));
    // If offset is not specified, shift the archive and discard used bytes
    if (offset == -1) pop_front(archive, sizeof(T));
    // Return the size of the type
    return sizeof(T);
}

/**
 * Serialize an entity into bytes
 * @param src to serialize
 * @param archive to serialize into
 * @param offset in archive to serialize into
 * @return size of the entity that was serialized in bytes
 */
static size_t SerializeEntity(const Entity *src, std::vector<uint8_t> &archive, size_t offset) {

    uint64_t commandBufferSize = src->commandBuffer.size();

    size_t bytes = sizeof(src->identifier) + sizeof(commandBufferSize)+commandBufferSize * sizeof(Command) +
                   sizeof(Command) +
                   sizeof(State) +
                   sizeof(State);

    archive.resize(archive.size() + bytes);

    Serialize(&src->identifier, archive, offset);
    offset += sizeof(src->identifier);

    Serialize(&commandBufferSize, archive, offset);
    offset += sizeof(commandBufferSize);

    for (auto &cmd : src->commandBuffer) {
        Serialize<Command>(&cmd, archive, offset);
        offset += sizeof(cmd);
    }

    Serialize<Command>(&src->currentCommand, archive, offset);
    offset += sizeof(src->currentCommand);

    Serialize<State>(&src->currentState, archive, offset);
    offset += sizeof(src->currentState);

    Serialize<State>(&src->predictedState, archive, offset);
    offset += sizeof(src->predictedState);

    return bytes;
}

/**
 * Deserialize bytes to an entity
 * @param result to serialize into
 * @param archive to deserialize from
 * @param offset in archive to deserialize from
 * @return size of the object that was deserialized in bytes
 */
static size_t DeserializeEntity(Entity *result, std::vector<uint8_t> &archive, size_t offset) {

    size_t bytes = 0;

    bytes += Deserialize<uint64_t>(&result->identifier, archive, offset + bytes);

    uint64_t commandBufferSize;
    bytes += Deserialize<uint64_t>(&commandBufferSize, archive, offset + bytes);

    for (auto i = 0; i < commandBufferSize; i++) {
        Command cmd;
        bytes += Deserialize<Command>(&cmd, archive, offset + bytes);
        result->commandBuffer.emplace_back(cmd);
    }

    bytes += Deserialize<Command>(&result->currentCommand, archive, offset + bytes);
    bytes += Deserialize<State>(&result->currentState, archive, offset + bytes);
    bytes += Deserialize<State>(&result->predictedState, archive, offset + bytes);

    return bytes;
}

#endif //SF2568_SERIALIZATION_H
