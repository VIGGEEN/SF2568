//
// Created by Viktor Meyer
//

#ifndef SF2568_STATICPARTITION_H
#define SF2568_STATICPARTITION_H

#include <utility>
#include <cmath>
#include "Entity.h"
#include "IOBus.h"
#include "Config.h"
#include "Serialization.h"
#include <fstream>
#include <unordered_map>

struct StaticPartition {

    // Partition rank
    int rank;

    // Output stream for logging
    std::ofstream log;

    // IOBus for inter-processor communication
    IOBus io;

    // Local buffer for entities
    std::unordered_map<uint64_t, Entity> _entities;

    // Partition bounds
    double minx, maxx, miny, maxy;

    // Current partition tick
    uint32_t worldTick = 0;

    // Partition initialization time
    double start_time;

    /**
     * Constructs a partition
     * @param rank of partition
     */
    explicit StaticPartition(int rank);

    /**
     * Destructor
     */
    ~StaticPartition();

    /**
     * Processes IO by checking IOBus for new data and sending buffered data
     */
    void ProcessIO();

    /**
     * Server main-loop
     */
    void ServerLoop();

    /**
     * Simulates the partition one tick
     */
    void Tick();

    /**
     * Calculate the correct cell for the corresponding coordinate
     * @param x coordinate-x
     * @param y coordinate-y
     * @return appropriate cell for coordinate
     */
    int CoordinateToCell(double x, double y) const;

    /**
     * Time elapsed since partition initialization
     * @return time elapsed since partition initialization
     */
    double ElapsedTime() const;

    /**
     * The target tick based on time elapsed, where the partition worldTick **should** be at
     * @return where the partition worldTick **should** be at
     */
    uint32_t TargetTick() const;

    /**
     * Center of this partition
     * @return Center coordinate of this partition
     */
    std::pair<double, double> center();

};

#endif //SF2568_STATICPARTITION_H
