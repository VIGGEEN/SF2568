//
// Created by viktorm
//

#ifndef SF2568_CONFIG_H
#define SF2568_CONFIG_H

#include <cstddef>
struct Config {

    /**
     * Initilize global config
     * @param nrpoc number of processes
     */
    static void Init(size_t nrpoc);

    // Specifies the number of microseconds to sleep, to emulate a real world scenario
    static constexpr size_t EXTRAWORK = 0;

    // Radius of each entity in the world
    static constexpr double ENTITYSIZE = 10;

    // Whether or not to enable collision between entities
    static constexpr bool COLLISION = true;

    // Number of entities to create in the world
    static constexpr size_t ENTITIES = 2048;

    // How often to update the targets for each entity
    static constexpr double TARGETTIMEOUT = 10.0;

    // Whether or not to draw targets in the world
    static constexpr bool DISABLETARGETDRAW = false;

    // Number of ticks to simulate per second
    static constexpr double TICKRATE = 60.0;

    // Bounds of the world
    static constexpr size_t DIM = 768;

    // Number of processes
    static size_t NPROC;

    // Number of processes per coordinate axis
    static size_t NAXIS;

    //Size of each cell when using static partitioning
    static size_t CELLSIZE;

    // Communication channel for primary entities
    static constexpr int CH_PRIMARYENTITY = 0;

    // Communication channel for secondary entities
    static constexpr int CH_SECONDARYENTITY = 3;

    // Communication channel for secondary entities
    static constexpr int CH_DESTROYENTITY = 4;

    // State communication channel
    static constexpr int CH_STATE = 1;

    // Command communication channel
    static constexpr int CH_COMMAND = 2;

};

#endif //SF2568_CONFIG_H
