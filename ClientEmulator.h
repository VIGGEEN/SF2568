//
// Created by viktorm
//

#ifndef SF2568_CLIENTEMULATOR_H
#define SF2568_CLIENTEMULATOR_H

#include <random>
#include "IOBus.h"
#include "Entity.h"
#include "Config.h"
#include "DynamicPartition.h"

/**
 * Emulates client behaviour by sending commands and receiving state updates from server
 */
struct ClientEmulator {

    // IOBus for inter-processor communication
    IOBus io;

    // Target positions for entities
    std::vector<std::pair<double, double>> entityTargets;

    // Current states for entities
    std::vector<State> entities;

    /**
     * Constructs a client emulator
     */
    ClientEmulator();

    /**
     * Create an entity
     */
    void CreateEntity();

    /**
     * Create an entity
     * @param batch to serialize into
     */
    void CreateEntity(IOBatch &batch);

    /**
     * Processes IO by checking IOBus for new data and sending buffered data
     */
    void ProcessIO();

    /**
     * Generates and sends commands for each entity that moves the entity towards its target position
     */
    void GenerateCommands();

    /**
     * Generates target positions for each entity
     */
    void GenerateTargets();

    /**
     * Client main-loop
     */
    void ClientLoop();

    /**
     * Generates a random 1D-coordinate within the world bounds
     * @return random 1D-coordinate
     */
    static double randomcoord();

};

#endif //SF2568_CLIENTEMULATOR_H
