//
// Created by viktorm
//

#ifndef SF2568_UTILITY_H
#define SF2568_UTILITY_H

/**
 * Pops the front n elements from container
 * @tparam T container type
 * @param container object
 * @param n elements to pop
 */
template<typename T>
static void pop_front(T &container, size_t n) {
    std::copy(container.begin() + n, container.end(), container.begin());
    container.resize(container.size() - n);
}

#endif //SF2568_UTILITY_H
