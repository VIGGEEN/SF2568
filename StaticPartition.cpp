//
// Created by Viktor Meyer
//

#include <mpi.h>
#include "StaticPartition.h"
#include "Config.h"
#include <iostream>
/**
 * Constructs a partition
 * @param rank of partition
 */
StaticPartition::StaticPartition(int rank) {

    // Caluculate partition boundaries
    this->rank = rank;
    this->minx = (rank % Config::NAXIS) * Config::CELLSIZE;
    this->maxx = this->minx + Config::CELLSIZE;
    this->miny = ((rank - (rank % Config::NAXIS)) / Config::NAXIS) * Config::CELLSIZE;
    this->maxy = this->miny + Config::CELLSIZE;
    this->start_time = MPI_Wtime();

    std::stringstream ss;

    ss << "STATIC";
    #ifdef SKEWED
    ss << "SKEWED";
    #elif HOTSPOT
    ss << "HOTSPOT";
    #else
    ss << "UNIFORM";
    #endif
    ss << "_";
    ss << Config::NPROC;
    ss << '_';
    ss << Config::ENTITIES;
    ss << "_";
    ss << rank;
    ss << ".csv";

    // Open a logfile
    log.open(ss.str());
}

/**
 * Server main-loop
 */
void StaticPartition::ServerLoop() {
    while (true) {

        // Start timer for tick
        auto start = MPI_Wtime();

        // Checking IOBus for new data and send buffered data
        ProcessIO();

        // Calculate if we can simulate next frame
        uint32_t frames = TargetTick() - worldTick;
        if (frames > 0) {
            Tick();
        }

#ifndef SERIAL
        // Synchronize processes
        MPI_Barrier(MPI_COMM_WORLD);
#endif

        // End timer
        auto end = MPI_Wtime();

        // Logging
        if (frames != 0) {
            log << TargetTick() << ',' << ((end - start) * 1000) << std::endl;
            if (1.0 / (end - start) < Config::TICKRATE) {
                std::cout << rank << "WARNING: CANNOT KEEP UP TICKRATE (" << (end - start) * 1000 << "ms)" << std::endl;
            }
        }
    }
}

/**
 * Simulates the partition one tick
 */
void StaticPartition::Tick() {

    // Create Archives to serialize into
    ARCHIVE stateArchive;

    #ifndef SERIAL
    // Create an IOBatch for transmission
    IOBatch primaryBatch(Config::CH_PRIMARYENTITY);
    IOBatch secondaryBatch(Config::CH_SECONDARYENTITY);
    IOBatch destroyBatch(Config::CH_DESTROYENTITY);
    #endif

    auto iterator = _entities.begin();
    // Iterate over all local entities
    while (iterator != _entities.end()) {
        auto &entity = *iterator;
        if (!entity.second.primary){
            //std::cout << rank <<" ITER SECONDARY " << entity.second.identifier << std::endl;
            iterator++;
            continue;
        }
        // Simulate the entity
        entity.second.Tick();
        // Check if collision is enabled
        if (Config::COLLISION) {
            // Iterate over all local entities
            for (auto &other: _entities) {
                // Check if current entity is equal to this, skip if so
                if (other.second.identifier == entity.second.identifier) continue;
                // Check if the two entities overlap (collide)
                if (other.second.distance(entity.second) < Config::ENTITYSIZE * Config::ENTITYSIZE) {
                    // Resolve the collision
                    double dx = entity.second.currentState.x - other.second.currentState.x;
                    double dy = entity.second.currentState.y - other.second.currentState.y;
                    double len = std::sqrt(dx * dx + dy * dy);
                    if(len == 0) {
                        dx = 0.1;
                        len = std::sqrt(dx * dx + dy * dy);
                        //std::cout << "WHAT THE FUK" << std::endl;
                    }
                    dx /= len;
                    dy /= len;
                    dx *= Config::ENTITYSIZE;
                    dy *= Config::ENTITYSIZE;
                    entity.second.currentState.x = other.second.currentState.x + dx;
                    entity.second.currentState.y = other.second.currentState.y + dy;
                }
            }
        }

        // Serialize the current state into state archive
        Serialize(&entity.second.currentState, stateArchive);

        #ifndef SERIAL

        // Find where the primary copy should be located
        auto primaryTarget = CoordinateToCell(iterator->second.currentState.x, iterator->second.currentState.y);

        auto oldSecondaries = iterator->second.secondaries;
        auto newSecondaries = std::vector<int>();

        // Compute the partitions that will need secondary copies
        if (int target = CoordinateToCell(iterator->second.currentState.x + Config::ENTITYSIZE,
                                          iterator->second.currentState.y); target != primaryTarget) {
            newSecondaries.emplace_back(target);
        }
        // Compute the partitions that will need secondary copies
        if (int target = CoordinateToCell(iterator->second.currentState.x - Config::ENTITYSIZE,
                                          iterator->second.currentState.y); target != primaryTarget) {
            newSecondaries.emplace_back(target);
        }
        // Compute the partitions that will need secondary copies
        if (int target = CoordinateToCell(iterator->second.currentState.x,
                                          iterator->second.currentState.y + Config::ENTITYSIZE); target != primaryTarget) {
            newSecondaries.emplace_back(target);
        }
        // Compute the partitions that will need secondary copies
        if (int target = CoordinateToCell(iterator->second.currentState.x,
                                          iterator->second.currentState.y - Config::ENTITYSIZE); target != primaryTarget) {
            newSecondaries.emplace_back(target);
        }

        iterator->second.secondaries = newSecondaries;

        // SEND DESTROY TO SECONDARIES NO LONGER RELEVANT
        auto it = oldSecondaries.begin();
        while (it != oldSecondaries.end()) {
            auto find = std::find_if(newSecondaries.begin(), newSecondaries.end(),
                                     [&](int candidate) { return *it == candidate; });
            if (find == newSecondaries.end() && *it != primaryTarget) {
                // Create an archive to serialize into
                auto &archive = destroyBatch.Send(*it);
                // Serialize entity into archive
                SerializeEntity(&(iterator->second), archive, archive.size());
                it = oldSecondaries.erase(it);
            }
            else {
                it++;
            }
        }

        // SEND SECONDARY ENTITIES TO SECONDARIES
        for (auto &target : newSecondaries) {
            // Create an archive to serialize into
            auto &archive = secondaryBatch.Send(target);
            // Serialize entity into archive
            SerializeEntity(&(iterator->second), archive, archive.size());
        }

        // Update primary if necessary
        if (primaryTarget != rank) {
            // Create an archive to serialize into
            auto &archive = primaryBatch.Send(primaryTarget);
            // Serialize entity into archive
            SerializeEntity(&(iterator->second), archive, archive.size());
            // Erase the entity from local buffer
            iterator = _entities.erase(iterator);
            continue;
        }
        #endif

        // Advance the iterator
        iterator++;
    }
    // Advance the world tick
    worldTick++;
    #ifndef SERIAL
    // Transfer the result archive to corresponding processors
    destroyBatch.Commit(io);
    secondaryBatch.Commit(io);
    primaryBatch.Commit(io);
    #endif

    // Transfer the state archive to client emulator process
    io.Send(std::move(stateArchive), Config::NPROC, Config::CH_STATE);
}

/**
 * Destructor
 */
StaticPartition::~StaticPartition() {
    log.flush();
    log.close();
}

/**
 * Time elapsed since partition initialization
 * @return time elapsed since partition initialization
 */
double StaticPartition::ElapsedTime() const {
    return MPI_Wtime() - start_time;
}

/**
 * The target tick based on time elapsed, where the partition worldTick **should** be at
 * @return where the partition worldTick **should** be at
 */
uint32_t StaticPartition::TargetTick() const {
    return static_cast<uint32_t >(ElapsedTime() * Config::TICKRATE);
}

/**
 * Center of this partition
 * @return Center coordinate of this partition
 */
std::pair<double, double> StaticPartition::center() {
    return std::make_pair(minx + (maxx - minx) / 2, miny + (maxy - miny) / 2);
}

/**
 * Processes IO by checking IOBus for new data and sending buffered data
 */
void StaticPartition::ProcessIO() {
    // Check IOBus for new data and sending buffered data
    auto incomingData = io.Update();
    // Process all incoming data
    for (auto &archive : incomingData) {
        //Check if archive tag
        if (archive->first.MPI_TAG == Config::CH_PRIMARYENTITY) {
            size_t offset = 0;
            // Deserialize an incoming primary entity
            while (offset < archive->second.size()) {
                Entity entity(0);
                offset += DeserializeEntity(&entity, archive->second, offset);
                entity.primary = true;
                //Update the local buffer
                _entities.insert_or_assign(entity.identifier, std::move(entity));
            }
        } else if (archive->first.MPI_TAG == Config::CH_SECONDARYENTITY) {
            size_t offset = 0;
            // Deserialize an incoming secondary entity
            while (offset < archive->second.size()) {
                Entity entity(0);
                offset += DeserializeEntity(&entity, archive->second, offset);
                entity.primary = false;
                //Update the local buffer
                _entities.insert_or_assign(entity.identifier, std::move(entity));
            }
        } else if (archive->first.MPI_TAG == Config::CH_DESTROYENTITY) {
            size_t offset = 0;
            // Deserialize an incoming destroy
            while (offset < archive->second.size()) {
                Entity entity(0);
                offset += DeserializeEntity(&entity, archive->second, offset);
                _entities.erase(entity.identifier);
            }
        } else if (archive->first.MPI_TAG == Config::CH_COMMAND) {
            size_t offset = 0;
            // Deserialize an incoming command
            while (offset < archive->second.size()) {
                Command cmd;
                offset += Deserialize<Command>(&cmd, archive->second, offset);
                // Find entity in local buffer
                auto it = _entities.find(cmd.identifier);
                if (it != _entities.end()) {
                    // Insert the new command into local entity buffer
                    it->second.commandBuffer.emplace_back(std::move(cmd));
                }
            }
        }
    }
}

/**
 * Calculate the correct cell for the corresponding coordinate
 * @param x coordinate-x
 * @param y coordinate-y
 * @return appropriate cell for coordinate
 */
int StaticPartition::CoordinateToCell(double x, double y) const {
    x = std::clamp(x, 1.0, static_cast<double>(Config::DIM - 1));
    y = std::clamp(y, 1.0, static_cast<double>(Config::DIM - 1));
    int dx = std::floor((x / static_cast<double>(Config::DIM)) * Config::NAXIS);
    int dy = std::floor((y / static_cast<double >(Config::DIM)) * Config::NAXIS);
    const int final = dy * Config::NAXIS + dx;
    return final;
}
