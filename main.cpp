#include <mpi.h>
#include "StaticPartition.h"
#include "ClientEmulator.h"
#include "Renderer.h"

int main(int argc, char **argv) {

    // Initialize the MPI environment
    MPI_Init(&argc, &argv);

    // Get the number of processes
    int nproc;
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    // Initialize global config
    Config::Init(nproc - 1);

    // Get the rank of the process
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank != nproc - 1) {
        #ifdef DYNAMIC
        // Create a dynamic partition
        DynamicPartition cell(rank);
        #else
        // Create a static partition
        StaticPartition cell(rank);
        #endif
        // Start server main-loop
        cell.ServerLoop();
    } else {
        // Create client emulator to emulate a real scenario
        ClientEmulator commandEmitter;
        // Create a renderer to visualize world and entities
        Renderer::Create(&commandEmitter);
        // Start client main-loop
        commandEmitter.ClientLoop();
    }

    // Finalize the MPI environment.
    MPI_Finalize();

}