//
// Created by viktorm
//

#ifndef SF2568_RENDERER_H
#define SF2568_RENDERER_H

#include "ClientEmulator.h"

#ifdef PIKSEL
#include <piksel/baseapp.hpp>
#define BLACK glm::vec4(0.117f, 0.152f, 0.180f, 1.0f)
#define RED glm::vec4(1.f, 0.247f, 0.203f, 1.0f)
#define GREEN glm::vec4(0.019f, 0.768f, 0.419f, 1.0f)
#define YELLOW glm::vec4(1.f, 0.658f, 0.003f, 1.0f)

class Renderer : piksel::BaseApp {
#else
class Renderer {
#endif
public:
    /**
     * Create a new renderer
     * @param ref to emulator for state information
     */
    static void Create(const ClientEmulator *ref) {
#ifdef PIKSEL
        // Create a new thread to not block the main-loops
        std::thread instance([ref](){
            Renderer renderer(ref);
            renderer.start();
        });
        instance.detach();
#endif
    }

private:
#ifdef PIKSEL

    // Reference to emulator for state information
    const ClientEmulator * _source;

    // Create renderer window
    explicit Renderer(const ClientEmulator *front) : piksel::BaseApp(Config::DIM, Config::DIM,
                                                                     "SF2568 Parallel Computations for Large-Scale Problems"),
                                                     _source(front) {}

    void draw(piksel::Graphics &g) {

        // Start timer
        auto start = MPI_Wtime();

        constexpr double DIST = Config::ENTITYSIZE*2;

        // Clear background
        g.background(BLACK);

        // Draw ellipses centered
        g.ellipseMode(piksel::DrawMode::CENTER);

        // Check if target drawing is enabled
        if (Config::ENTITIES <= 512 || Config::DISABLETARGETDRAW) {
            // Iterate over all entities
            for (size_t i = 0; i < _source->entities.size(); i++) {
                // Get data
                auto &state = _source->entities[i];
                auto &goal = _source->entityTargets[i];

                // Check if target has been reached
                auto dist = (goal.first - state.x) * (goal.first - state.x) +
                            (goal.second - state.y) * (goal.second - state.y);

                // Draw target information
                g.fill(BLACK);
                g.ellipse(goal.first, goal.second, 5.f, 5.f);
                g.fill((dist < DIST * DIST ? GREEN : YELLOW) - glm::vec4(0, 0, 0, 0.9f));
                g.ellipse(goal.first, goal.second, DIST, DIST);
            }
        }

        // Iterate over all entities
        for (size_t i = 0; i < _source->entities.size(); i++) {
            // Get data
            auto &state = _source->entities[i];
            auto &goal = _source->entityTargets[i];

            // Check if target has been fulfilled
            auto dist =
                    (goal.first - state.x) * (goal.first - state.x) + (goal.second - state.y) * (goal.second - state.y);

            // Draw entity information
            g.fill(dist < DIST * DIST ? GREEN : YELLOW);
            g.ellipse(state.x, state.y, Config::ENTITYSIZE, Config::ENTITYSIZE);
        }

        // Disable fill
        g.noFill();

        // Draw static partition spatial regions
        for (size_t row = 0; row < Config::NPROC / 2; row++) {
            for (size_t col = 0; col < Config::NPROC / 2; col++) {
                g.rect(col * Config::CELLSIZE, row * Config::CELLSIZE, Config::CELLSIZE, Config::CELLSIZE);
            }
        }

        // End timer
        auto end = MPI_Wtime();

        // Check if rendering loop is within acceptable timings
        if (1.0 / (end - start) < Config::TICKRATE) {
            std::cout << "RENDERER CANNOT KEEP UP WITH TICKRATE" << std::endl;
        }
    }

#endif
};

#endif //SF2568_RENDERER_H
