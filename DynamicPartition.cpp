//
// Created by viktorm
//

#include "DynamicPartition.h"

/**
 * Constructs a partition
 * @param rank of partition
 */
DynamicPartition::DynamicPartition(int rank) : rank(rank), start_time(MPI_Wtime()) {
    std::stringstream ss;

    ss << "DYNAMIC";
    #ifdef SKEWED
    ss << "SKEWED";
    #elif HOTSPOT
    ss << "HOTSPOT";
    #else
    ss << "UNIFORM";
    #endif
    ss << "_";
    ss << Config::NPROC;
    ss << '_';
    ss << Config::ENTITIES;
    ss << "_";
    ss << rank;
    ss << ".csv";

    // Open a logfile
    log.open(ss.str());
}

/**
 * Destructor
 */
DynamicPartition::~DynamicPartition() {
    log.flush();
    log.close();
}

/**
 * Processes IO by checking IOBus for new data and sending buffered data
 */
void DynamicPartition::ProcessIO() {
    // Check IOBus for new data and sending buffered data
    auto incommingData = io.Update();
    // Process all incoming data
    for (auto &archive : incommingData) {
        //Check if archive tag
        if (archive->first.MPI_TAG == Config::CH_PRIMARYENTITY) {
            size_t offset = 0;
            // Deserialize an incoming entity
            while (offset < archive->second.size()) {
                Entity entity(0);
                offset += DeserializeEntity(&entity, archive->second, offset);
                //Update the local buffer
                _entities.emplace_back(std::move(entity));
            }
        } else if (archive->first.MPI_TAG == Config::CH_COMMAND) {
            size_t offset = 0;
            // Deserialize an incoming command
            while (offset < archive->second.size()) {
                Command cmd;
                offset += Deserialize<Command>(&cmd, archive->second, offset);
                // Find entity in local buffer
                auto it = std::find_if(_entities.begin(), _entities.end(),
                                       [&](const Entity &cand) {
                                           return cand.identifier == cmd.identifier;
                                       });
                if (it != _entities.end()) {
                    // Insert the new command into local entity buffer
                    it->commandBuffer.emplace_back(std::move(cmd));
                }
            }
        }
    }
}

/**
 * Server main-loop
 */
void DynamicPartition::ServerLoop() {
    while (true) {

        // Wait until a tick has passed
        while (TargetTick() - worldTick <= 0);

        // Start timer for tick
        auto start = MPI_Wtime();

        // Synchronize processes
        MPI_Barrier(MPI_COMM_WORLD);

        // Check if master process
        if (IsMaster()) {
            // Checking IOBus for new data and send buffered data
            ProcessIO();
            // Construct a graph to compute weakly connected components (AOI groups)
            WCC::Graph g(_entities.size());
            // Iterate over all entities
            for (size_t a = 0; a < _entities.size(); a++) {
                for (size_t b = a; b < _entities.size(); b++) {
                    // Check if entities are within AOI of eachother
                    if (_entities[a].isAOI(_entities[b])) {
                        // Add an edge between the two vertices in the graph
                        g.insertEdge(a, b);
                    }
                }
            }

            // Compute weakly connected components in graph
            auto components = g.ComputeComponents();

            // Sort the components by count of entities as LPT heuristic
            std::sort(components.begin(), components.end(),
                      [](const dynamic_bitset<> &lhs, const dynamic_bitset<> &rhs) {
                          return lhs.count() > rhs.count();
                      });

            // Construct schedule
            typedef std::pair<size_t, std::vector<size_t>> SCHED;
            std::vector<SCHED> schedule(Config::NPROC);

            // Iterate over all components
            for (size_t i = 0; i < components.size(); i++) {
                auto &component = components[i];
                // Find the least loaded entry in schedule (LPT)
                auto min = std::min_element(schedule.begin(), schedule.end(),
                                            [](const SCHED &lhs, const SCHED &rhs) {
                                                return lhs.first < rhs.first;
                                            });
                // Add the component to entry in schedule
                min->first += component.size();
                min->second.emplace_back(i);
            }

            // Create an IOBatch for transmission
            IOBatch batch(Config::CH_PRIMARYENTITY);

            // Iterate over each entry in schedule
            for (size_t i = 0; i < schedule.size(); i++) {
                SCHED &sched = schedule[i];
                // Allocate an archive for transmission to processor **i**
                auto &archive = batch.Send(i);
                // Iterate over all elements in schedule entry
                for (auto element : sched.second) {
                    auto &component = components[element];
                    auto ipos = component.find_first();
                    while (ipos < component.size() && ipos != dynamic_bitset<>::npos) {
                        // Serialize entity information into archive
                        SerializeEntity(&(_entities[ipos]), archive, archive.size());
                        ipos = component.find_next(ipos);
                    }
                }
            }

            // Clear local entities
            _entities.clear();

            // Commit the batch and transmit it to processors
            batch.Commit(io);
        }

        // Synchronize processors
        MPI_Barrier(MPI_COMM_WORLD);

        // Checking IOBus for new data and send buffered data
        ProcessIO();

        // Simulate the world one tick
        Tick();

        // End timer
        auto end = MPI_Wtime();

        // Log the time elapsed for the simulation tick
        log << TargetTick() << ',' << ((end - start) * 1000) << std::endl;

        // Clear local entities, we receive new next tick
        _entities.clear();
    }
}

/**
 * Simulates the partition one tick
 */
void DynamicPartition::Tick() {

    // Create Archives to serialize into
    ARCHIVE stateArchive;
    ARCHIVE resultArchive;

    // Create an IOBatch for transmission
    IOBatch batch(Config::CH_PRIMARYENTITY);

    auto iterator = _entities.begin();
    // Iterate over all local entities
    while (iterator != _entities.end()) {
        auto &entity = *iterator;

        // Simulate the entity
        entity.Tick();

        // Check if collision is enabled
        if (Config::COLLISION) {
            // Iterate over all local entities
            for (auto &other: _entities) {
                // Check if current entity is equal to this, skip if so
                if (other.identifier == entity.identifier) continue;
                // Check if the two entities overlap (collide)
                if (other.distance(entity) < Config::ENTITYSIZE * Config::ENTITYSIZE) {
                    // Resolve the collision
                    double dx = entity.currentState.x - other.currentState.x;
                    double dy = entity.currentState.y - other.currentState.y;
                    double len = std::sqrt(dx * dx + dy * dy);
                    dx /= len;
                    dy /= len;
                    dx *= Config::ENTITYSIZE;
                    dy *= Config::ENTITYSIZE;
                    entity.currentState.x = other.currentState.x + dx;
                    entity.currentState.y = other.currentState.y + dy;
                }
            }
        }

        // Serialize the current state into state archive
        Serialize(&entity.currentState, stateArchive);

        // Serialize the current entity into result archive
        SerializeEntity(&(entity), resultArchive, resultArchive.size());

        // Advance the iterator
        iterator++;

    }

    // Advance the world tick
    worldTick++;

    // Transfer the result archive to master processor
    io.Send(std::move(resultArchive), Master(), Config::CH_PRIMARYENTITY);

    // Transfer the state archive to client emulator process
    io.Send(std::move(stateArchive), Config::NPROC, Config::CH_STATE);
}

/**
 * Gets the master process rank
 * @return master process rank
 */
int DynamicPartition::Master() {
    return 0;
}

/**
 * Whether the current partition is running on master process
 * @return true if current partition is running master process
 */
bool DynamicPartition::IsMaster() const {
    return rank == Master();
}

/**
 * Time elapsed since partition initialization
 * @return time elapsed since partition initialization
 */
double DynamicPartition::ElapsedTime() const {
    return MPI_Wtime() - start_time;
}

/**
 * The target tick based on time elapsed, where the partition worldTick **should** be at
 * @return where the partition worldTick **should** be at
 */
uint32_t DynamicPartition::TargetTick() const {
    return static_cast<uint32_t >(ElapsedTime() * Config::TICKRATE);
}
