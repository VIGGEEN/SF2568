# SF2568 World Simulation
## Compiling
- mkdir build
- cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=mpicc -DCMAKE_CXX_COMPILER=mpic++
- make

## Configuring CMakeLists.txt
- set(HOTSPOT OFF/ON) whether or not to use hotspot entity distribution
- set(SKEWED OFF/ON) whether or not to use skewed entity distribution
- If none of HOTSPOT or SKEWED are set, uniform distribution is used
- set(DYNAMIC OFF/ON) wheter or not to use dynamic partitioning
- set(SERIAL OFF/ON) wheter or not to use serial exection, not compatible with DYNAMIC ON

## Enabling rendering
- Rendering requires piksel library (git submodule update --init --recursive to fetch)
- set(RENDER ON) to enable rendering